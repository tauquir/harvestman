import urllib2, urlparse, lxml
from bs4 import BeautifulSoup
from robotparser import RobotFileParser

from harvestman.models import UserDescription
from harvestman.dbapi import create_record, url_exists, \
    limit_exceeded

ROBOT_PARSER = RobotFileParser()
REPOSITORY = {}
USER_PROFILES = []
BLACKLIST = [
    'https://vimeo.com/',
    'https://vimeo.com/plus',
    'https://vimeo.com/join',
    'https://vimeo.com/staffpicks',
    'https://vimeo.com/movies',
    'https://vimeo.com/categories',
    'https://vimeo.com/channels',
    'https://vimeo.com/groups',
    'https://vimeo.com/couchmode',
    'https://vimeo.com/upload',
    'https://vimeo.com/join',
    'https://vimeo.com/enhancer',
    'https://vimeo.com/videoschool',
    'https://vimeo.com/musicstore',
    'https://vimeo.com/creativecommons',
    'https://vimeo.com/creatorservices',
    'https://vimeo.com/log_in',
    'https://vimeo.com/love',
    'http://vimeo.com/love',
    'http://vimeo.com/about',
    'https://vimeo.com/about',
    'https://ianlaser.com',
    'https://ianlaser.com/',
    'http://vimeo.com/',
    'http://vimeo.com/plus',
    'http://vimeo.com/join',
    'http://vimeo.com/staffpicks',
    'http://vimeo.com/movies',
    'http://vimeo.com/categories',
    'http://vimeo.com/channels',
    'http://vimeo.com/groups',
    'http://vimeo.com/couchmode',
    'http://vimeo.com/upload',
    'http://vimeo.com/join',
    'http://vimeo.com/enhancer',
    'http://vimeo.com/videoschool',
    'http://vimeo.com/musicstore',
    'http://vimeo.com/creativecommons',
    'http://vimeo.com/creatorservices',
    'http://vimeo.com/log_in',
    'http://ianlaser.com',
    'http://ianlaser.com/',
    ]

class Crawler(object):
    """ A Very basic crawler, No Exception handling. Just To
    update database with required data.
    """
    
    def __init__(self, url):
        self.url = url
        self.timeout = 30
        self.robot_parser = ROBOT_PARSER
        self.repository = REPOSITORY
        self.user_profiles = USER_PROFILES
        self.visited = 0
        self.blacklist = BLACKLIST
        self.base_url = 'http://vimeo.com'
        
    def _find(self, url):
        """ Already indexed url
        """
        if url in self.repository:
            return True
        return False

    def _black_list(self, url):
        """ Black list url
        """
        if url in self.blacklist:
            return True
        return False

    def _url_exist(self, url):
        """ Db call to check that url already index or not. Seconf measure
        to make sure that multiple process can run smoothly.
        """
        
        exist = url_exists(url)
        if exist:
            return True
        return False
    
    def _is_valid_url(self, url):
        """ Ugly method to avoid unecessary url to get indexed
        """
        parsed_url = urlparse.urlparse(url)
        if not (parsed_url.scheme == 'http' or parsed_url.scheme == 'https'):
            return False
        if 'couchmode' in url:
            return False
        if 'help' in url:
            return False
        if 'faq' in url:
            return False
        if 'groups' in url:
            return False
        if 'join' in url:
            return False
        if 'movies' in url:
            return False
        if 'videos' in url:
            return False
        if 'likes' in url:
            return False
        if 'channel' in url:
            return False
        if 'sort' in url:
            return False
        if not 'vimeo.com' in url:
            return False
        if 'developer' in url:
            return False
        if 'blog' in url:
            return False
        if 'terms' in url:
            return False
        if 'forgot_password' in url:
            return False
        if 'job' in url:
            return False
        if 'site_map' in url:
            return False
        if 'album' in url:
            return False
        if 'home' in url:
            return False
        if 'tools' in url:
            return False
        if 'forums' in url:
            return False
        if 'feedmanager' in url:
            return False
        if 'settings' in url:
            return False
        if 'perks' in url:
            return False
        if 'explore' in url:
            return False
        if 'claim' in url:
            return False
        if '#' in url:
            return False
        if 'portfolios' in url:
            return False
        if 'stats' in url:
            return False
        if 'pro' in url:
            return False
        if 'business' in url:
            return False
        if 'dmca' in url:
            return False
        if 'privacy' in url:
            return False
        
        return True
    
    def _fetch_url(self, url):
        """ Fetch url with google bot signature
        """
        from time import sleep
        sleep(1)
        bot = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
        header_payload = { 'User-Agent' : bot}
        req = urllib2.Request(url, headers=header_payload)
        return urllib2.urlopen(req).read()

    def _is_video_a_staff_pick(self, video_url):
        """ Does video a staff pick
        """
        staff_base_channel_url = 'https://vimeo.com/channels/staffpicks'
        probable_staff_pick_url = '%s%s' % (staff_base_channel_url, video_url)
        try:
            self._fetch_url(probable_staff_pick_url)
            return True
        except urllib2.URLError:
            return False

    def _get_all_video_of_user(self, page_urls):
        """ Get all video by user
        """
        videos_url = []
        for page_url in page_urls:
            content = self._fetch_url(self.base_url+page_url)
            soup = BeautifulSoup(content, 'lxml')
            video_list = soup.findAll(attrs={'id':'browse_list'})
            if(len(video_list) > 0):
                listing = video_list[0]
                [videos_url.append(anchor['href']) for anchor in listing.findAll('a', href=True)]
        return videos_url

    def _fetch_page_info(self, url):
        """ Fetch page information
        """
        try:
            content = self._fetch_url(url)
        except urllib2.URLError:
            return
        except urllib2.HTTPError:
            return
        
        soup = BeautifulSoup(content, 'lxml')
        data = soup.findAll('header')[1]
        namediv = data.findAll(attrs={'itemprop':'name'})[0]
        name = namediv.text

        plus = soup.findAll(attrs={'class':'badge_plus'})
        if(len(plus) > 0):
            is_paid = True
        else:
            is_paid = False
        uploaded = soup.findAll(attrs={'class':'empty'})
        if(len(uploaded) > 0):
            is_uploaded = True
            if 'Videos' in uploaded[0].text:
                is_uploaded = False
        else:
            is_uploaded = False
            
        is_staff_pick = False
        if is_uploaded:
            new_url = url.rstrip("/")
            video_url = new_url + '/videos'
            content = self._fetch_url(video_url)
            soup = BeautifulSoup(content, 'lxml')

            pagination = soup.findAll(attrs={'class':'pagination'})
            if(len(pagination) > 0):
                from sets import Set
                videos = Set()
                pagination = pagination[0]
                [videos.add(anchor['href']) for anchor in pagination.findAll('a', href=True)]

                videos_url = self._get_all_video_of_user(videos)
                for v_url in videos_url:
                    staff_pick = self._is_video_a_staff_pick(v_url)
                    if staff_pick:
                        is_staff_pick = True
                        break

        create_record(**{'name':name, 'url':url, 'is_paid':is_paid, 'is_staff_pick':is_staff_pick, 'is_uploaded':is_uploaded})
        return
        
        
    def _fetch_from_profile_page(self, url):
        """ Check whether url is of user profile or not.
        """
        content = self._fetch_url(url)
        soup = BeautifulSoup(content, 'lxml')
        is_profile = soup.findAll(attrs={'content':'profile'})
        if(is_profile):
            return True
        return False
    
    def parse(self, url=None):
        """ BeautifulSoup parse the element.
        """
        if(not url):
            url = self.url
            
        content = self._fetch_url(url)
        soup = BeautifulSoup(content, 'lxml')
        
        for tag in soup.find_all('a', href=True):
            new_url = urlparse.urljoin(url, tag['href'])
            if(self._find(new_url)):
                continue
            if(not self._is_valid_url(new_url)):
                continue
            if(self._black_list(new_url)):
                continue
            if(self._url_exist(new_url)):
                continue
            if(limit_exceeded()):
                break
            print new_url
            self.visited = 1
            self.repository[new_url] = self.visited

            is_profile = self._fetch_from_profile_page(new_url)
            if is_profile:
                print 'USER PROFILE : - ' + new_url
                self._fetch_page_info(new_url)
                
            self.user_profiles.append(new_url)
            try:
                yield self.parse(new_url)
            except KeyError:
                pass
            
