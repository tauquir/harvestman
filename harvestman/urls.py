import os
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'harvestman.views.index_page', name='index-page'),
    url(r'search$', 'harvestman.views.search', name='search'),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',  {'document_root': os.path.join(os.path.dirname(__file__), "../static")}),
)
