import json
from django.http import HttpResponse, HttpResponseRedirect, \
    HttpResponseNotAllowed
from django.core.urlresolvers import reverse
from django.core import serializers
from django.shortcuts import render

from harvestman.dbapi import search_user

def index_page(request, template_name='index.html'):
    """ Home page.
    """
    return render(request, template_name);

def search(request):
    """ Search based on query string.
    """
    if request.method != 'GET' or not request.is_ajax():
        return HttpResponseNotAllowed('GET')
    
    query = request.GET.get('q')
    search_type =  request.GET.get('type')
    
    if not query :
        return HttpResponse(json.dumps({'success': False, 'message': 'Empty query string'}), mimetype='application/json')

    response = search_user(query, search_type)
    return HttpResponse(serializers.serialize('json', response))
    
