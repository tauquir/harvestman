from django.db import models
from django.utils.timezone import now

class UserDescription(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(max_length=255)
    is_paid = models.BooleanField(default=False)
    is_staff_pick = models.BooleanField(default=False)
    is_uploaded = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self,):
        return self.name
    
    
