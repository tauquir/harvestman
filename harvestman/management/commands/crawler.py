from django.core.management.base import BaseCommand, CommandError
from harvestman.crawler import Crawler

class Command(BaseCommand):
    args = '<start_url>'
    help = 'Crawl till user count reaches to number 5000.'
    
    def handle(self, *args, **options):
        if len(args) > 0:
            c = Crawler(args[0])
        else:
            c = Crawler('https://vimeo.com/eterea')
        items = c.parse()
        for item in items:
            try:
                print item.next()
            except StopIteration:
                pass
                
        
