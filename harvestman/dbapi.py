import operator
from django.db.models import Q
from harvestman.models import UserDescription


def create_record(**kwargs):
    """ Create a record in DB.
    """
    name = kwargs.get('name')
    url = kwargs.get('url')
    is_paid = kwargs.get('is_paid')
    is_staff_pick = kwargs.get('is_staff_pick')
    is_uploaded = kwargs.get('is_uploaded')

    record = UserDescription.objects.create(name=name, url=url, is_paid=is_paid, \
                                                is_staff_pick=is_staff_pick, is_uploaded=is_uploaded)
    record.save()

def url_exists(url):
    try:
        return UserDescription.objects.get(url=url)
    except UserDescription.DoesNotExist:
        return

def limit_exceeded():
    count = UserDescription.objects.count()
    if count == 5000:
        return True
    return False

def search_user(search_user, search_type='all'):
    """ Search query string.
    """
    qs = [Q(name__icontains=x) for x in search_user.split(' ')]
    qset = reduce(operator.or_, qs)

    if(search_type == 'paying'):
        qset = qset & (Q(is_paid=True))
        
    if(search_type == 'uploaded'):
        qset = qset & (Q(is_uploaded=True))
        
    if(search_type == 'staffpick'):
        qset = qset & (Q(is_staff_pick=True))
        
    return UserDescription.objects.filter(qset)
                                          
    
    
