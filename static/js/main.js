var Harvestman = window.Harvestman || {};

(function(){
    var $H = window.Harvestman; 
    $H.data = {};

    $H.utils = {
	doMassage:function(text) {
	    if(text === false){
		return 'N';
	    }

	    if(text === true) {
		return 'Y';
	    }

	    return text;
	},
	doAjax:function(url,type,data, link) {
	    $.ajax({
		url:url,
		type:type,
		data:data,
		dataType:'json',
		success:function(data){
		    console.log(data);
		    if(data.success === false) {
			$("#results").text('');
			$(".alert").addClass('hide');
			$("#query-box").css("border-color", "red");
			alert(data.message);
		    } else {
			$("#results").text('');
			$("#query-box").removeAttr('style');
			var count = data.length;
			$(".alert").removeClass('hide');
			$("#count").text(count);
			if(count > 0) {
			    $(".breadcrumb").removeClass('hide');
			    $("#"+link).addClass('active');
			} else {
			    $(".breadcrumb").addClass('hide');
			}
			$.each(data, function(d){
			    var response = data[d]['fields'];
			    result = '<div class="result_row"><a href="'+response.url+'" class="darkblue link font_20">'+response.name+'</a><p class="font_14"> Paying: '+$H.utils.doMassage(response.is_paid)+'</p><p class="font_14"> Staff Pick: '+$H.utils.doMassage(response.is_staff_pick)+'</p><p class="font_14"> Uploaded: '+$H.utils.doMassage(response.is_uploaded)+'</p></div>';
			    $("#results").append(result);
			});
		    }
		}
	    });
	}
    };
	
})();

$(function(){
    $H = Harvestman;
    $H.data = {'url':'/search'}
    $('#search-btn').live('click', function(e){
	e.preventDefault();
	var _query = $("#query-box").val();
	var _type = $("#filter").val();
	var url = $H.data.url;
	var _data = {'q': _query, 'type': _type}
	$H.utils.doAjax(url, 'GET', _data, 'all');
    });

    $('.filterclick').live('click', function(e) {
	e.preventDefault();
	$('.filterclick').removeClass('active');
	var _query = $("#query-box").val();
	var _type = this.id;
	var url = $H.data.url;
	var _data = {'q': _query, 'type': _type}
	$H.utils.doAjax(url, 'GET', _data, _type);
    });
});
